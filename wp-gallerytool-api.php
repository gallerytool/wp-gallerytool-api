<?php

/**
 * Plugin Name:       GalleryTool API for WordPress
 * Plugin URI:        https://gallerytool.com
 * Description:       List your GalleryTool resources via WordPress.
 * Version:           1.1.1
 * Author:            GalleryTool
 * Author URI:        https://gallerytool.com
 * License:           MIT
 * License URI:       https://opensource.org/licenses/MIT
 * Text Domain:       gt
 * Domain Path:       /languages/
 * Requires at least: 5.1
 * Requires PHP:      7.0
 */

// Pull in the autoloader
require_once __DIR__.'/autoload.php';

// Boot the plugin
GalleryTool\Api\Plugin::boot();
