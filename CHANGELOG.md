# Changelog

## v1.1.1 (2020-07-14)
### Changed
- Changed root URL

## v1.1.0 (2020-05-21)
### Added
- Added docs

### Removed
- Remove `json_encode` before serialization

### Fixed
- Cleaned up update hook
- Fixed typo in description

## v1.0.0 (2020-05-13)
- Initial release
