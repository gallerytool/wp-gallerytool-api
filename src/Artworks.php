<?php

namespace GalleryTool\Api;

use ArrayIterator;
use Exception;

class Artworks
{
    /**
     * A new method.
     *
     * @return void
     */
    public function register()
    {
        register_post_type('artwork', [
            'labels' => [
                'name'  => _x('Artworks', 'Post type general name', 'gt'),
                'singular_name' => _x('Artwork', 'Post type singular name', 'gt'),
                'menu_name' => _x('Artworks', 'Admin Menu text', 'gt'),
            ],
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => false,
            'query_var' => true,
            'rewrite' => ['slug' => 'artwork'],
            'capability_type' => 'post',
            'capabilities' => ['create_posts' => false],
            'map_meta_cap' => false,
            'has_archive' => true,
            'hierarchical' => false,
            'menu_position' => null,
            'supports' => [],
        ]);
    }

    /**
     * Import the artworks.
     *
     * @return void
     */
    public static function import()
    {
        static::delete();

        try {
            $iterator = new ArrayIterator(Requester::artworks());

            foreach ($iterator as $artwork) {
                static::insert($artwork);
            }
        } catch (Exception $e) {
            //
        }

        wp_redirect(admin_url('edit.php?post_type=artwork'));
        exit;
    }

    /**
     * Insert the given artwork.
     *
     * @return void
     */
    protected static function insert(array $artwork)
    {
        if ($artwork['image']) {
            $artwork['meta'] = array_merge($artwork['meta'] ?? [], [
                'image_url' => $artwork['image']['full_url'],
                'artist' => $artwork['artist']['translation']['content']['name'] ?? '',
            ]);
        }

        wp_insert_post([
            'post_type' => 'artwork',
            'post_title' => $artwork['translation']['content']['title'] ?? "Artwork #{$artwork['id']}",
            'post_content' => $artwork['translation']['content']['description'] ?? '',
            'post_status' => 'publish',
            'post_author' => get_current_user_id(),
            'meta_input' => $artwork['meta'] ?? [],
        ]);
    }

    /**
     * Clear the artworks before import.
     *
     * @return void
     */
    public static function delete()
    {
        global $wpdb;

        $wpdb->query($wpdb->prepare(
            "DELETE a,b FROM {$wpdb->prefix}posts a LEFT JOIN {$wpdb->prefix}postmeta b ON (a.ID = b.post_id) WHERE a.post_type = '%s';",
            ['artwork']
        ));
    }

    /**
     * Boot the artworks.
     *
     * @return void
     */
    public static function boot()
    {
        (new static)->registerHooks();
    }

    /**
     * Register the hooks.
     *
     * @return void
     */
    public function registerHooks()
    {
        add_action('init', [$this, 'register']);
    }
}
