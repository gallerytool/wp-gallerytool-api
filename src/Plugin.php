<?php

namespace GalleryTool\Api;

class Plugin
{
    /**
     * The plugin version.
     *
     * @var string
     */
    const VERSION = '1.1.1';

    /**
     * The plugin slug.
     *
     * @var string
     */
    const SLUG = 'wp-gallerytool-api/wp-gallerytool-api.php';

    /**
     * Boot the plugin.
     *
     * @return void
     */
    public static function boot()
    {
        add_filter('plugin_action_links_'.static::SLUG, [__CLASS__, 'addLinks']);
        load_plugin_textdomain('gt', false, basename(dirname(__DIR__)).'/languages');

        add_action('admin_post_gallery_tool_import', [Artworks::class, 'import']);
        add_action('admin_post_gallery_tool_delete', function () {
            Artworks::delete();
            wp_redirect(admin_url('edit.php?post_type=artwork'));
            exit;
        });

        Settings::boot();
        Artworks::boot();
        Updater::boot();
    }

    /**
     * Add plugin settings link.
     *
     * @param  array  $links
     * @return array
     */
    public static function addLinks($links)
    {
        return array_merge($links, [
            sprintf('<a href="%s">%s</a>', admin_url('admin.php?page=gallery-tool'), __('Settings')),
        ]);
    }
}
