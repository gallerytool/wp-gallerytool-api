<?php

namespace GalleryTool\Api;

use Exception;

class Requester
{
    /**
     * The HTTP headers.
     *
     * @var array
     */
    protected $headers = [];

    /**
     * The base URL.
     *
     * @var string
     */
    protected $baseUrl = 'https://app.gallerytool.com/wp/';

    /**
     * Create a new requester instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->headers['Authorization'] = 'Bearer '.get_option('gallery_tool_token');
    }

    /**
     * Send a GET request.
     *
     * @param  string  $uri
     * @param  array  $params
     * @return array
     *
     * @throws \Exception
     */
    public function get($uri, array $params = [])
    {
        $uri = $this->baseUrl.$uri.http_build_query($params, '', '&', PHP_QUERY_RFC3986);

        $response = wp_remote_get($uri, ['headers' => $this->headers]);

        if (is_wp_error($response)) {
            throw new Exception('HTTP request failed.');
        }
        return json_decode(wp_remote_retrieve_body($response), true);
    }

    /**
     * Get the lists.
     *
     * @return array
     */
    public static function lists()
    {
        return (new static)->get('lists');
    }

    /**
     * Get the artworks of the selected list.
     *
     * @return array
     */
    public static function artworks()
    {
        $list = get_option('gallery_tool_list');

        return (new static)->get("lists/{$list}");
    }
}
