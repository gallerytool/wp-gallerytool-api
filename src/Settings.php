<?php

namespace GalleryTool\Api;

use Exception;

class Settings
{
    /**
     * Register the settings.
     *
     * @return void
     */
    public function register()
    {
        add_menu_page(
			__('GalleryTool', 'gt'), __( 'GalleryTool', 'gt'), 'manage_options',
			'gallery-tool', [$this, 'render'], 'dashicons-share-alt', 3
        );

        add_submenu_page(
            'gallery-tool', __('Artworks'), __('Artworks'),
            'manage_options', 'edit.php?post_type=artwork'
        );
    }

    /**
     * Render the settings page.
     *
     * @return void
     */
    public function render()
    {
        $error = false;
        $token = get_option('gallery_tool_token');

        if (! $lists = get_transient('gallery_tool_lists')) {
            try {
                $lists = Requester::lists();
                set_transient('gallery_tool_lists', $lists, HOUR_IN_SECONDS);
            } catch (Exception $e) {
                $error = true;
            }
        }

        $lists = $lists ?: [];

        $options = [
            'token' => $token,
            'list' => get_option('gallery_tool_list'),
        ];

        include_once __DIR__.'/../includes/fields.php';
    }

    /**
     * Register the settings.
     *
     * @return void
     */
    public function settings()
    {
        register_setting('gallery_tool_settings', 'gallery_tool_list');
        register_setting('gallery_tool_settings', 'gallery_tool_token');
    }

    /**
     * Boot the module.
     *
     * @return void
     */
    public static function boot()
    {
        (new static)->registerHooks();
    }

    /**
     * Register the hooks.
     *
     * @return void
     */
    public function registerHooks()
    {
        add_action('admin_menu', [$this, 'register']);
        add_action('admin_init', [$this, 'settings']);
        add_action('update_option_gallery_tool_token', function () {
            delete_transient('gallery_tool_lists');
        }, 10, 0);
    }
}
