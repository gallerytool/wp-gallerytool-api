<div class="wrap">
    <?php if ($error || ! $options['token']): ?>
        <div class="error notice">
            <p><?php _e('There has been an error. Please check your token!', 'gt'); ?></p>
        </div>
    <?php endif; ?>
    <h1><?php _e('GalleryTool Settings', 'gt'); ?></h1>
    <form method="POST" action="options.php">
        <?php settings_fields('gallery_tool_settings'); ?>
        <?php do_settings_sections('gallery_tool_settings'); ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row"><?php _e('Token', 'gt'); ?></th>
                <td>
                    <input type="password" name="gallery_tool_token" value="<?php echo esc_attr($options['token']); ?>">
                </td>
            </tr>
            <tr valign="top">
                <th scope="row"><?php _e('List', 'gt'); ?></th>
                <td>
                    <?php if ($options['token'] && ! $error): ?>
                        <select name="gallery_tool_list">
                            <option disabled selected><?php _e('Select a list', 'gt'); ?></option>
                            <?php foreach ($lists as $list): ?>
                                <option
                                    value="<?php echo $list['id'] ?>"
                                    <?php if ($list['id'] == $options['list']): ?>
                                        selected
                                    <?php endif; ?>
                                ><?php echo $list['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    <?php else: ?>
                        <span><?php _e('Please enter a valid token to fetch the lists.', 'gt'); ?></span>
                    <?php endif; ?>
                </td>
            </tr>
        </table>
        <?php submit_button(); ?>
    </form>
    <hr>
    <h1>
        <?php _e('Artworks', 'gt'); ?>
        <?php if (! $options['list']): ?>
            <p><?php _e('Please select a list to import artworks.', 'gt'); ?></p>
        <?php else: ?>
            <p class="submit">
                <button form="import-form" type="submit" class="button button-primary">
                    <?php _e('Import Artworks', 'gt'); ?>
                </button>
                <button form="delete-form" type="submit" class="button">
                    <?php _e('Delete Artworks', 'gt'); ?>
                </button>
            </p>
            <form id="import-form" action="<?php echo admin_url('admin-post.php'); ?>" method="POST" style="display:none;">
                <input type="hidden" name="action" value="gallery_tool_import">
            </form>
            <form id="delete-form" action="<?php echo admin_url('admin-post.php'); ?>" method="POST" style="display:none;">
                <input type="hidden" name="action" value="gallery_tool_delete">
            </form>
        <?php endif; ?>
    </h1>
</div>
