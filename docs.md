# GalleryTool WordPress Plugin

Version: v1.1.0

Date: 2020-05-21

## Installation and configuration

### Installing the plugin

First of all, download the plugin as a `.zip` file from the [GitLab repository](https://gitlab.com/gallerytool/wp-gallerytool-api).
Then install the plugin on your WordPress site by manually uploading the `.zip` file.

### Configuring the plugin

After successfully installing and activating the plugin, naviatie to its configuration page.
First of all, paste the `token` that you copied from the GalleryTool database's settings page and **hit save**.

> Please pay attention to paste the token without extra whitespaces.

We save the token to the `gallery_tool_token` option.

After saving the token, select a list you want to import and **hit save** again.

We save the list to the `gallery_tool_list` option.

> Please note, to reduce HTTP queries we cache the lists for **one hour**.
> You may obtain a transient manager plugin to handle cache.
> We save the lists to the `gallery_tool_lists` transient.

## Importing and deleting

After saving the list, two buttons will appear where you can perform the importing and deleting.

### Importing the artworks from the list

To import the artworks click to the *"Import artworks"* button.
This will start importing the artworks from the GalleryTool database.

> Please note, before every import all the previously imported artworks will be deleted, to prevent duplications.

After the import is done, you will be redirected to the listing of the artworks.

> **Importnat**: You cannot edit or delete these artworks on the WordPress admin surface.

### Deleting the artworks

To delete the artworks click to the *"Delete artworks"* button.

## Using the artworks in your theme or plugin

The artworks are registered with the custom `artwork` post type.

The `title` and the `content` will be extracted from the GalleryTool database's default translation.
All the other values will be stored as meta values.

> You can retreive the post meta using the `get_post_meta` function.

The list of the meta values can be different, but here is a list of the (mostly) available metas:

- `inventory_number`
- `reproduction`
- `signiture`
- `signature`
- `edition`
- `year`
- `transport`
- `framed_size`
- `size`
- `stockist_price`
- `reduced_price`
- `price`
- `image_url`

> Please note, the `image_url` is the URL of the main artwork image.
> It will be pulled if the arworks has images on the GalleryTool admin.
>
> **Important**: The WordPress media manager will not contain the image itself.
> You can access only via URL using the `image_url` option.
